@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>News</h1>

            <a class="btn" href="{{ url('admin/dashboard') }}">List</a>
            <a class="btn btn-primary" href="{{ url('admin/dashboard/create') }}">Create</a>

            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ url('admin/dashboard') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Thumbnail</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="file" id="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label">Short Description</label>
                    <div class="col-sm-10">
                        <textarea rows="6" name="short_description" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label">Content</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="content"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <input type="submit" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
      $(document).ready(function() {
        $('#content').summernote({
          height: 300,
          callbacks: {
            onImageUpload: function(files, editor, $editable) {
              sendFile(files[0],editor,$editable);
            }
          }
        });
      });

      function sendFile(file,editor,welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
          url: "/summernote/image-upload",
          data: data,
          cache: false,
          contentType: false,
          processData: false,
          type: 'POST',
          success: function(data){
            $('#content').summernote("insertImage", data, 'filename');
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
          }
        });
      }
    </script>
@endsection
