@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>News</h1>

            <a class="btn btn-primary" href="{{ url('admin/dashboard') }}">List</a>
            <a class="btn" href="{{ url('admin/dashboard/create') }}">Create</a>

            <hr>

            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col" width="50px">#</th>
                    <th scope="col" width="200px">Created at</th>
                    <th scope="col">Title</th>
                    <th scope="col">Short Description</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($news as $newsSingle)
                    <tr>
                        <th scope="row">{{ $newsSingle->id }}</th>
                        <td>{{ $newsSingle->created_at }}</td>
                        <td><a href="{{ url('admin/dashboard/'.$newsSingle->id.'/edit') }}">{{ $newsSingle->title }}</a></td>
                        <td>{{ $newsSingle->short_description }}</td>
                        <td class="text-right">
                            <a href="{{ url('news/'.$newsSingle->slug) }}" target="_blank">
                                <i class="fa fa-anchor"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $news->links() }}

        </div>
    </div>
@endsection
