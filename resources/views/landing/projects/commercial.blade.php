@extends('landing.layouts.master')

@section('content')
    <section id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center">
                    <h3 class="uppercase color-primary mb0 mb-xs-24">Projects</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="projects pt0">
        <div class="container">
            <div class="row pb24">
                <div class="col-sm-12 text-center">
                    <ul class="filters mb0">
                        <a class="btn btn-filled btn-lg mb0" href="{{ url('/projects/commercial') }}">Comercial</a>
                        <a class="btn btn-lg mb0" href="{{ url('/projects/residential') }}">Residential</a>
                        <a class="btn btn-lg mb0" href="{{ url('/projects/developer') }}">Developer</a>
                    </ul>
                </div>
            </div>
            <br/>
            <!--end of row-->
            <div class="row masonry-loader">
                <div class="col-sm-12 text-center">
                    <div class="spinner"></div>
                </div>
            </div>
            <div class="row masonry masonryFlyIn">
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/finna/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Finna</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/le-ciel-bleu/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Le Ciel Bleu</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/mentos/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Mentos</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/ipiems/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Ipiems</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/sejahtera-motor/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Sejahtera Motor</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 masonry-item commercial" data-filter="Commercial">
                    <div class="image-tile inner-title text-center">
                        <a href="#">
                            <img alt="Pic" src="{{ asset('img/commercial/ivy-school/main.jpg') }}" />
                            <div class="title">
                                <h5 class="uppercase mb0">Ivy School</h5>
                                <span>Commercial</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
