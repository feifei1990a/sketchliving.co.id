<div class="col-md-3 hidden-sm">
    <div class="widget">
        <h6 class="title">Recent News</h6>
        <hr>
        <ul class="link-list recent-posts">
            @foreach($news as $newsSingle)
                <li>
                    <a href="{{ url('/news/'.$newsSingle->slug) }}">{{ $newsSingle->title }}</a>
                    <span class="date">{{ $newsSingle->created_at->format('d M Y H:i') }}</span>
                </li>
            @endforeach
        </ul>
    </div>
</div>
