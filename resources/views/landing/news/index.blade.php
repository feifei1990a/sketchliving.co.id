@extends('landing.layouts.master')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24">
                    @foreach($news as $newsSingle)
                    <div class="post-snippet mb64">
                        <a href="{{ url('/news/'.$newsSingle->slug) }}">
                            @if(Storage::exists($newsSingle->thumbnail))
                                <img width="100%" height="100%" class="mb24" src="{{ Storage::url($newsSingle->thumbnail) }}" alt="" height="300px">
                            @endif
                        </a>
                        <div class="post-title">
                            <span class="label">{{ $newsSingle->created_at->format('d M') }}</span>
                            <a href="{{ url('/news/'.$newsSingle->slug) }}">
                                <h4 class="inline-block">{{ $newsSingle->title }}</h4>
                            </a>
                        </div>
                        <hr>
                        <p>
                            {{ $newsSingle->short_description }}
                        </p>
                        <a class="btn btn-sm" href="{{ url('/news/'.$newsSingle->slug) }}">Read More</a>
                    </div>
                    @endforeach
                    @if($news->count() == 0)
                    <div class="fullscreen post-snippet mb64">
                        No News
                    </div>
                    @endif

                    <!--end of post snippet-->
                    <div class="text-center">
                        <ul class="pagination">
                            {{ $news->links() }}
                        </ul>
                    </div>
                </div>

                @include('landing.news.sidebar', ['news' => $news])
            </div>
        </div>
    </section>
@endsection
