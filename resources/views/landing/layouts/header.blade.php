<div class="nav-container">
    <a id="top"></a>
    <nav class="absolute {{ request()->path() == '/' ? 'transparent' : '' }}">
        <div class="nav-bar">
            <div class="module left">
                <a href="{{ url('/') }}">
                    <img class="logo" alt="Sketchliving" src="{{ asset('img/logo-w.png') }}" />
                </a>
            </div>
            <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                <i class="ti-menu"></i>
            </div>
            <div class="module-group right">
                <div class="module left">
                    <ul class="menu">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ url('/#company-overview') }}">Company Overview</a>
                        </li>
                        <li>
                            <a href="{{ url('projects') }}">Our Project</a>
                        </li>
                        <li>
                            <a href="{{ url('news') }}">News</a>
                        </li>
                        <li>
                            <a href="{{ url('contact') }}">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
